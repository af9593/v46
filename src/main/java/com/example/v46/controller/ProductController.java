package com.example.v46.controller;

import com.example.v46.model.Product;
import com.example.v46.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;


@RestController
@RequestMapping("/api/product")
public class ProductController {

    @Autowired
    ProductRepository productRepository;

    @GetMapping()
    public List<Product> getProducts(){
        return productRepository.findAll();
    }

    @GetMapping("{id}")
    public Product getProduct(@PathVariable long id ){  return productRepository.getById(id); }

    @PostMapping
    public void addProduct(@RequestBody Product product) {
        Optional<Product> existingProduct =  productRepository.findById(product.getProduct_id());
        if(existingProduct.isEmpty())  productRepository.save(product);
    }

    @DeleteMapping("/{id}")
    public void removeProduct(@PathVariable long id ) {
        try {
            Product existingProduct  =  productRepository.findAll().stream().filter(p -> p.getProduct_id() == id).findFirst().get();
            productRepository.deleteById(id);
        }catch (NoSuchElementException e){
            System.out.println(e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public void updateProduct(@RequestBody Product product , @PathVariable long id ){
        try {
            Product existingProduct  =  productRepository.findAll().stream().filter(p -> p.getProduct_id() == id).findFirst().get();
            productRepository.save(product);
        }catch (NoSuchElementException e){
            System.out.println(e.getMessage());
        }
    }
}
