package com.example.v46.controller;

import com.example.v46.model.Customer;
import com.example.v46.model.Order;
import com.example.v46.repository.CustomerRepository;
import com.example.v46.repository.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

@RestController
@RequestMapping("/api/customer")
public class CustomerController {
    
    @Autowired
    CustomerRepository customerRepository;

    @Autowired
    OrderRepository orderRepository;

    @GetMapping()
    public List<Customer> getCustomers(){
        return customerRepository.findAll();
    }



    @GetMapping("{id}")
    public Customer getCustomer(@PathVariable long id ){  return customerRepository.getById(id); }

    @GetMapping("{id}/orders")
    public List<Order> getCustomerOrders(@PathVariable long id){
        System.out.println("HAHAHHAHAHHAHAHAHA");
        return  orderRepository.findAll().stream().filter(o -> o.getCustomer().getCustomer_id() == id).findAny().stream().toList();
    }

    @PostMapping
    public void addCustomer(@RequestBody Customer customer) {
        Optional<Customer> existingCustomer =  customerRepository.findById(customer.getCustomer_id());
        if(existingCustomer.isEmpty())  customerRepository.save(customer);
    }

    @DeleteMapping("/{id}")
    public void removeCustomer(@PathVariable long id ) {
        try {
            Customer existingCustomer  =  customerRepository.findAll().stream().filter(c -> c.getCustomer_id() == id).findFirst().get();
            customerRepository.deleteById(id);
        }catch (NoSuchElementException e){
            System.out.println(e.getMessage());
        }

    }

    @PutMapping("/{id}")
    public void updateCustomer(@RequestBody Customer customer , @PathVariable long id ){
        try {
            Customer existingCustomer = customerRepository.findAll().stream().filter(c -> c.getCustomer_id() == id ).findFirst().get();

            customerRepository.save(customer);
        /*
            customerRepository
            .save(
                    customerRepository.findAll().stream()
                    .filter(c -> c.getCustomer_id() == id)
                    .findFirst().get()
            );
          */

        }catch (NoSuchElementException e){
            System.out.println(e.getMessage());
        }
    }
}
