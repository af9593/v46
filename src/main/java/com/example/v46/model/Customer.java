package com.example.v46.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.*;
import javax.persistence.Table;
import java.util.List;

@Entity
@Table(name="CUSTOMER_TABLE")
@Getter
@Setter
public class Customer {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long customer_id;
    @Column(name = "NAME")
    private String name;
    @Column(name = "ADDRESS")
    private String address;

}
