package com.example.v46.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;
import javax.persistence.*;

//@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})


@Entity(name="PRODUCT_TABLE")
@Table
@Getter
@Setter
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long product_id;

    @Column(name = "LABEL")
    private String label ;

    @Column(name ="DESCRIPTION")
    private String description ;

    @Column(name="PRICE")
    private int price;

}

