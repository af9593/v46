package com.example.v46;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class V46Application {

	public static void main(String[] args) {
		SpringApplication.run(V46Application.class, args);
	}

}
